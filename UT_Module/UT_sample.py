import unittest
from unittest.mock import Mock, patch
import pytest
from Module.sample import LogicalMethod

@pytest.fixture
def logical_method_instance():
    return LogicalMethod()


def test_method_01_case1(logical_method_instance):

    para1 = 1
    para2 = 2

    return_value = logical_method_instance.method_01(para1, para2)
    assert return_value == 1


def test_method_01_case2(logical_method_instance):

    para1 = 2
    para2 = 1

    return_value = logical_method_instance.method_01(para1, para2)
    assert return_value == 2


def test_method_01_case3(logical_method_instance):

    para1 = 2
    para2 = 2

    return_value = logical_method_instance.method_01(para1, para2)
    assert return_value == 3


def test_method_02_case1(logical_method_instance):

    with patch('Module.Utility.BasicMethod.print_something') as mock_print_something:
        para1 = 1
        para2 = 1
        logical_method_instance.method02(para1, para2)
        mock_print_something.assert_called_once()


def test_method_02_case2(logical_method_instance):

    with patch('Module.Utility.BasicMethod.add_something') as mock_add_something:
        para1 = 2
        para2 = 1
        logical_method_instance.method02(para1, para2)
        mock_add_something.assert_called_once()


def test_method_02_case3(logical_method_instance):

    with patch('Module.Utility.BasicMethod.subtract_something') as mock_subtract_something:
        para1 = 2
        para2 = 2
        logical_method_instance.method02(para1, para2)
        mock_subtract_something.assert_called_once()


def test_method_03_case1(logical_method_instance):
    para1 = 3
    para2 = 1
    para3 = 5

    logical_method_instance.method03(para1, para2, para3)


def test_method_03_case2(logical_method_instance):
    with patch('Module.Utility.BasicMethod.subtract_something') as mock_subtract_something:
        para1 = 1
        para2 = 1
        para3 = 5

        logical_method_instance.method03(para1, para2, para3)


def test_method_03_case3(logical_method_instance):
    with patch('Module.Utility.BasicMethod.subtract_something') as mock_subtract_something:
        para1 = 15
        para2 = 13
        para3 = 11

        logical_method_instance.method03(para1, para2, para3)
        mock_subtract_something.assert_called_once()


def test_method_03_case4(logical_method_instance):
    with patch('Module.Utility.BasicMethod.subtract_something') as mock_subtract_something:
        para1 = 15
        para2 = 13
        para3 = 10

        logical_method_instance.method03(para1, para2, para3)





