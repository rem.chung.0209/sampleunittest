import sys
import os


# add path to directory Module to sys.path
module_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'Module'))
sys.path.insert(0, module_path)