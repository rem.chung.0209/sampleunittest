from Module.Utility import BasicMethod

class LogicalMethod(object):
    def method_01(self, para01, para02) -> int:
        if para01 == 1:
            return 1
        elif para02 == 1:
            return 2
        else:
            return 3

    def method02(self, arg01, arg02) -> None:
        if arg01 == 1:
            BasicMethod.print_something(BasicMethod())
        elif arg02 == 1:
            print('sample')
            BasicMethod.add_something(BasicMethod())
        else:
            BasicMethod.subtract_something(BasicMethod())

    def method03(self, arg01, arg02, arg03):
        if ((arg01 > arg02) or (arg01 > arg03)) and (arg01 - arg02) > 1:
            if arg02 > arg03:
                if arg03 > 10:
                    BasicMethod.subtract_something(BasicMethod())













