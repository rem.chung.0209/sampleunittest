


class BasicMethod(object):

    def print_something(self):

        print('Test print method')

        return 1

    def add_something(self):

        a = 1
        b = 2

        return a + b

    def subtract_something(self):

        a = 2
        b = 1

        return a - b
